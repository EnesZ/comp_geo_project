import pygame as pg
from random import randint,uniform
import collections
#screen
WIDTH = 600
HEIGHT = 400

FPS = 10
POLYGON = [(10,10),(200,250),(400,20),(500,300),(50,350)]

# colors
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
YELLOW = (255, 255, 0)
DARKGRAY = (40, 40, 40)

MAX_VELOCITY = 2
MIN_VELOCITY = 0.1
MAX_ALTITUDE = 21
MIN_ALTITUDE = 5
ARIVAL_TIME = 10
MAX_NUMBER_OF_FLIGHTS = 5
MIN_NUMBER_OF_FLIGHTS = 2

SAFE_ZONE = 20
PLANE_SIDE = 20

pg.font.init()
FONT = pg.font.SysFont("Consolas", 10)




class Plane(pg.sprite.Sprite):
    def __init__(self,path):
        pg.sprite.Sprite.__init__(self)
        #objekat sa sirinim i visinom, moze i slika da se stavi
        self.image = pg.Surface((PLANE_SIDE,PLANE_SIDE))
        self.plane_color = YELLOW
        self.image.fill(self.plane_color)
        self.rect = self.image.get_rect()
        self.position = path[0]
        self.rect.center = pg.math.Vector2(self.position.x, self.position.y)
        self.path = path
        self.desired_point_index = 1
        self.arrived = False
        self.velocity_magnitude = uniform(MIN_VELOCITY,MAX_VELOCITY)
        self.velocity = pg.Vector3(0,0,0)
        self.sum = pg.math.Vector3(0,0,0)
        self.count = 0
        self.land = False
    #ispisuje visinu aviona u kvadratu
    def show_text(self):
        text = FONT.render(str(round(self.position.z,1)), True, BLACK)
        text_rect = text.get_rect(center=(PLANE_SIDE / 2, PLANE_SIDE / 2))
        self.image.fill(self.plane_color)
        self.image.blit(text, text_rect)
    # vraca putanju u obliku za iscrtavanje
    def get_path(self):
        path = []
        for p in self.path:
            path.append((p.x,p.y))
        return path
    # vraca zeljenu tacku za iscrtavanje
    def get_desired_point(self):
        v=self.path[self.desired_point_index]
        return [int(v.x),int(v.y)]
    # vraca poziciju aviona u potrebnom obliku za iscrtavanje kruga
    def get_position(self):
        return [int(self.position.x),int(self.position.y)]

    def update(self):
        if self.land==True:
            return
        desired_point = self.path[self.desired_point_index]
        if(self.position.distance_squared_to(desired_point)<=self.velocity_magnitude):
            if(self.desired_point_index==len(self.path)-1):
                self.arrived = True
                return
            else:
                self.desired_point_index+=1
                desired_point = self.path[self.desired_point_index]
        velocity = self.calc_velocity(desired_point)
        self.velocity = velocity

        self.position += velocity
        if self.position.z<0:
            self.position.z=0
            self.land=True
        self.rect.center = pg.math.Vector2(self.position.x,self.position.y)
        self.show_text()



    def calc_velocity(self,desired_point):
        velocity = desired_point - self.position
        velocity.scale_to_length(self.velocity_magnitude)

        if self.count>0:
            self.sum/=self.count
            velocity = self.sum - velocity
            velocity.scale_to_length(self.velocity_magnitude)
            self.desired_point_index = len(self.path)-1
        return velocity
    @staticmethod
    def intersection(plane_list):
        plane_list = sorted(plane_list,key = lambda plane: plane.position.x)
        belt = collections.deque()
        for plane in plane_list:
            plane.count=0
            plane.sum=pg.math.Vector3(0,0,0)
            while True:
                if belt:
                    p_in_belt = belt.popleft()
                    if plane.position.x - p_in_belt.position.x > 2*SAFE_ZONE:
                        continue
                    belt.appendleft(p_in_belt)
                break

            for p_in_belt in belt:
                d = plane.position.distance_to(p_in_belt.position)
                if d< 2*SAFE_ZONE:

                    diff = plane.position-p_in_belt.position
                    plane.sum+=diff
                    p_in_belt.sum+= (-1*diff)
                    plane.count+=1
                    p_in_belt.count+=1

            belt.append(plane)


def generate_route():
    # 0 - externi, 1 - interni, 2 - poluinterni
    flight = randint(0,2)
    if flight==0:
        p1_x,p1_y = generate_point_on_edge(POLYGON)
        p2_x,p2_y = generate_point_on_edge(POLYGON)
        altitude = uniform(MIN_ALTITUDE,MAX_ALTITUDE)
        v1 = pg.math.Vector3(p1_x,p1_y,altitude)
        v2 = pg.math.Vector3(p2_x,p2_y,altitude)
        path = [v1,v2]
    elif flight==1:
        #p1----p3---(p4)---p2
        p1_x,p1_y = generate_point_inside_poly(POLYGON)
        p2_x,p2_y = generate_point_inside_poly(POLYGON)
        p3_x,p3_y = generate_point_on_edge(POLYGON,[(p1_x,p1_y),(p2_x,p2_y)])
        altitude = uniform(MIN_ALTITUDE, MAX_ALTITUDE)
        v1 = pg.math.Vector3(p1_x,p1_y,0)
        v2 = pg.math.Vector3(p2_x,p2_y,0)
        v3 = pg.math.Vector3(p3_x,p3_y,altitude)
        path = [v1,v3]
        #if flight is not "too short"
        if randint(0,1)==0:
            p4_x,p4_y = generate_point_on_edge(POLYGON,[(p3_x,p3_y),(p2_x,p2_y)])
            v4 = pg.math.Vector3(p4_x,p4_y,altitude)
            path.append(v4)
        path.append(v2)
    else:
        p1_x,p1_y = generate_point_inside_poly(POLYGON)
        p2_x,p2_y = generate_point_on_edge(POLYGON)
        altitude = randint(MIN_ALTITUDE,MAX_ALTITUDE)
        v1 = pg.math.Vector3(p1_x,p1_y,0)
        v2 = pg.math.Vector3(p2_x,p2_y,altitude)
        #outside
        path=[v1]
        # if flight is not "too short"
        if randint(0, 1) == 0:
            p3_x, p3_y = generate_point_on_edge(POLYGON,[(p1_x, p1_y), (p2_x, p2_y)])
            v3 = pg.math.Vector3(p3_x, p3_y, altitude)
            path.append(v3)
        path.append(v2)
        if randint(0,1)==0:
            #inside
            path.reverse()

    return Plane(path)


def generate_point_on_edge(polygon,edge=None):
    # if edge == none we choose random edge from polygon
    if edge==None:
        rand_vertex_index = randint(0, len(POLYGON)-1)
        p1 = polygon[rand_vertex_index-1]
        p2 = polygon[rand_vertex_index]
    else:
        p1 = edge[0]
        p2 = edge[1]
    rand_lambda = uniform(0, 1)
    p_x = p1[0]*rand_lambda + p2[0]*(1-rand_lambda)
    p_y = p1[1]*rand_lambda + p2[1]*(1-rand_lambda)
    return p_x,p_y
def generate_point_inside_poly(poly):
    min_x = poly[0][0]
    max_x = poly[0][0]
    min_y = poly[0][1]
    max_y = poly[0][1]

    for point in poly[1:]:
        if point[0]<min_x:
            min_x=point[0]
        if point[0]>max_x:
            max_x=point[0]
        if point[1]<min_y:
            min_y=point[1]
        if point[1]>max_y:
            max_y=point[1]

    found = False
    while not found:
        rand_x = uniform(min_x,max_y)
        rand_y = uniform(min_y,max_y)
        ref_p = (max_x+100,min_y)
        rand_p = (rand_x,rand_y)
        if is_inside_poly(rand_p,ref_p,poly):
            found=True

    return rand_x,rand_y

def is_inside_poly(rand_p,ref_p,poly):
    inter_num = 0
    for i in range(len(poly)):
        o1 = ori(poly[i - 1], poly[i], rand_p)
        o2 = ori(poly[i - 1], poly[i], ref_p)

        o3 = ori(rand_p, ref_p, poly[i - 1])
        o4 = ori(rand_p, ref_p, poly[i])
        if o1 == o2 == 0:
            # we wont consider points if they are collinear
            break
        if o1 != o2:
            if o3 == 0:
                # [rand_p,ref_p] intersects vertex poly[i-1]
                # we need to check neighbors of poly[i-1]
                if ori(rand_p, ref_p, poly[i - 2]) != o4:
                    inter_num += 1
                continue
            if o4 == 0:
                # [rand_p,ref_p] intersects vertex poly[i]
                if i == len(poly) - 1:
                    k = 0
                else:
                    k = i
                if ori(rand_p, ref_p, poly[k]) != o3:
                    inter_num += 1
                continue
            if o3 != o4:
                inter_num += 1
    if inter_num % 2 == 1:
        return True
    else:
        return False
def ori(p1,p2,p3):
    # x1(y2-y3)+x2(y3-y1)+x3(y1-y2)
    temp = p1[0]*(p2[1]-p3[1]) + p2[0]*(p3[1]-p1[1]) + p3[0]*(p1[1]-p2[1])
    if temp>0:
        return 1
    elif temp<0:
        return -1
    else:
        return 0

def gen_n_routes(existing_routes,n):
    num_of_tries = 0
    i=0
    while i<n:
        num_of_tries+=1
        if num_of_tries>100:
            print("Too many planes in the space")
            break
        p = generate_route()
        correct_route = True
        for plane in existing_routes:
            if p.position.distance_to(plane.position) < 2*SAFE_ZONE:
                correct_route = False
                break
        if correct_route==True:
            i+=1
            #new_routes.add(p)
            existing_routes.add(p)





pg.init()

screen = pg.display.set_mode((WIDTH, HEIGHT))
clock = pg.time.Clock()


all_sprites = pg.sprite.Group()
gen_n_routes(all_sprites,randint(MIN_NUMBER_OF_FLIGHTS,MAX_NUMBER_OF_FLIGHTS))
running = True
show_path = True
start_time = pg.time.get_ticks()
while running:
    clock.tick(FPS)
    for event in pg.event.get():
        if event.type == pg.MOUSEBUTTONDOWN:
            x,y = event.pos
            for plane in all_sprites:
                if plane.rect.collidepoint(x,y):
                    all_sprites.remove(plane)
                    break
        if event.type == pg.KEYDOWN:
            if event.key == pg.K_ESCAPE:
                running = False
            if event.key == pg.K_p:
                if show_path:
                    show_path = False
                else:
                    show_path = True

    screen.fill(DARKGRAY)
    pg.draw.polygon(screen, WHITE, POLYGON)
    if start_time:
        time_since_enter = (pg.time.get_ticks() - start_time) / 1000
        message = 'Seconds since enter: ' + str(time_since_enter)
        screen.blit(FONT.render(message, True, WHITE), (2, 0))
        if time_since_enter > ARIVAL_TIME:
            start_time = pg.time.get_ticks()
            for p in all_sprites:
                p.land=False
            gen_n_routes(all_sprites,randint(MIN_NUMBER_OF_FLIGHTS, MAX_NUMBER_OF_FLIGHTS))

    Plane.intersection(all_sprites)
    all_sprites.update()
    for plane in all_sprites:

        if show_path:
            pg.draw.lines(screen,RED,False,plane.get_path())
            pg.draw.circle(screen,GREEN,plane.get_desired_point(),2)
        pg.draw.circle(screen,BLUE,plane.get_position(),SAFE_ZONE,1)
        if plane.arrived==True:
            all_sprites.remove(plane)
    screen.blit(FONT.render("Number of planes: {}".format(len(all_sprites)), True, WHITE), (2, 20))
    screen.blit(FONT.render("Click on plane to remove", True, RED), (2, HEIGHT-20))

    all_sprites.draw(screen)
    pg.display.flip()

pg.quit()